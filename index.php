<!DOCTYPE html>
<html>
<head>
	<title>Http Angular</title>
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
	<link rel="stylesheet" type="text/css" href="search.css">
</head>
<body ng-app="myApp" ng-controller="dataCtrl">
	<h1>Data Gempa Terkini BMKG</h1>
	<input type="text" ng-model="cari" placeholder="Cari Data">
	<table border="1">
		<tr>
			<th>No</th>
			<th>Tanggal</th>
			<th ng-click="orderByMe('Jam')">Jam</th>
			<th class="magnitude" ng-click="orderByMe('Magnitude')">Skala</th>
			<th ng-click="orderByMe('Kedalaman')">Kedalaman</th>
			<th ng-click="orderByMe('Lintang')">Lintang</th>
			<th ng-click="orderByMe('Bujur')">Bujur</th>
			<th ng-click="orderByMe('Wilayah')">Wilayah</th>
			<th>Kordinat</th>
		</tr>
		<tr ng-repeat="x in gempa | filter:cari | orderBy:myOrderBy">
			<td>{{$index + 1}}</td>
			<td>{{x.Tanggal}}</td>
			<td>{{x.Jam}}</td>
			<td><b>{{x.Magnitude}}</b></td>
			<td>{{x.Kedalaman}}</td>
			<td>{{x.Lintang}}</td>
			<td>{{x.Bujur}}</td>
			<td>{{x.Wilayah}}</td>
			<td>{{x.point.coordinates}}</td>
		</tr>
	</table>
	<script>
		var app = angular.module('myApp', []);
		app.controller('dataCtrl', function($scope, $http) {
			$http.get("gempa.php").then(function(response) {
				$scope.gempa = response.data.gempa;
			});
			$scope.orderByMe = function(x) {
				$scope.myOrderBy = x;
			};
		});
	</script>
</body>
</html>